## Step 1
    Take clone
    Link : https://gitlab.com/bhavinlogicrays/payment-getways.git
    Copy : cp .env.example .env
    Run : php artisan key:generate
    Set .env file and create database in phpmyadmin

## Step 2
    Run : php artisan migrate --seed command

## Step 3
    Run : php artisan serve

## Razorpay Payment Getway Demo
    - Set Secret Key 
        "key": "rzp_test_MNOF9V98nGfvhp",
    - For access demo 
        Link : http://127.0.0.1:8000/razorpayment

## Stripe Payment Getway
    Add Below Variable in .env file and set key
        -> STRIPE_PUBLISHABLE
        -> STRIPE_STRIPE_SECRET

    - For access demo 
        Link : http://127.0.0.1:8000/seller/1

## Paystack Payment Getway
    - Set Secret Key 
        key: 'pk_test_9d707c4beb75cb68a291ec5bda4324ce68193681',
    - For access demo 
        Link : http://127.0.0.1:8000/paystack