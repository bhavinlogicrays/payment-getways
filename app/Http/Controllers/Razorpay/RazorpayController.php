<?php

namespace App\Http\Controllers\Razorpay;

use App\Http\Controllers\Controller;
use App\Models\Razorpay;
use Illuminate\Http\Request;

class RazorpayController extends Controller
{
    public function index()
    {
        return view('razorpay.index');
    }

    public function razorPaySuccess(Request $request)
    {
        $data = [
            'user_id' => '1',
            'product_id' => $request->product_id,
            'r_payment_id' => $request->payment_id,
            'amount' => $request->amount,
        ];

        $getId = Razorpay::insertGetId($data);

        $arr = array('msg' => 'Payment successfully credited', 'status' => true);

        return Response()->json($arr);
    }

    public function RazorThankYou()
    {
        return view('razorpay.thankyou');
    }
}
