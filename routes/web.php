<?php

use App\Http\Controllers\Paystack\PaystackController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Razorpay\RazorpayController;
use App\Http\Controllers\Stripe\StripeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Razorpay
Route::get('/razorpayment', [RazorpayController::class, 'index'])->name('razorpay');
Route::get('paysuccess', [RazorpayController::class, 'razorPaySuccess']);
Route::get('razor-thank-you', [RazorpayController::class, 'RazorThankYou']);

//Stripe
Route::get('seller/{id}', [StripeController::class, 'showProfile'])->name('seller.profile');
Route::get('stripe/{id}', [StripeController::class, 'redirectToStripe'])->name('redirect.stripe');
Route::get('connect/{token}', [StripeController::class, 'saveStripeAccount'])->name('save.stripe');
Route::get('charge/{id}', [StripeController::class, 'purchase'])->name('complete.purchase');

Route::get('paystack', [PaystackController::class, 'index']);
