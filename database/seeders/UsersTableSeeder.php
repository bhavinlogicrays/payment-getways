<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Dhaval',
                'email' => 'hiram.roob@example.org',
                'avatar' => 'https://randomuser.me/api/portraits/men/69.jpg',
                'stripe_connect_id' => 'acct_1KrEqq2R4NyQmdcA',
                'completed_stripe_onboarding' => false,
                'password' => Hash::make('Test@123'),
            ],
            [
                'name' => 'Cheyanne Schumm Sr.',
                'email' => 'santino22@example.net',
                'avatar' => 'https://randomuser.me/api/portraits/men/15.jpg',
                'stripe_connect_id' => 'acct_1Kg5cD2QgnY2HHeg',
                'completed_stripe_onboarding' => true,
                'password' => Hash::make('Test@123'),
            ],
            [
                'name' => 'Maximillia Predovic IV',
                'email' => 'antonia.eichmann@example.org',
                'avatar' => 'https://randomuser.me/api/portraits/women/5.jpg',
                'stripe_connect_id' => 'acct_1Kg5UR2SEbJwtXC3',
                'completed_stripe_onboarding' => true,
                'password' => Hash::make('Test@123'),
            ]
        ];
        User::insert($user);
    }
}
